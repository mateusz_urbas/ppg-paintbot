import { Injectable } from '@angular/core';
import { Message, MessageType } from './message';

@Injectable({
  providedIn: 'root'
})
export class ChatBotService {

  isChatBotActive = true;
  conversation = [];

  constructor() { }

  activeChatBot() {
    this.isChatBotActive = true;
  }

  enableChatBot() {
    this.isChatBotActive = false;
  }

  sentMessage(message) {
    this.conversation.push(message);
    this.receiveMessage(message);
  }

  receiveMessage(message) {
    const answer: Message = {
      message: 'Odpowiedz',
      time: Date(),
      type: MessageType.receivedTextMessage
    };

    setTimeout(() => {
      this.conversation.push(answer);
    }, 500);
  }

}
