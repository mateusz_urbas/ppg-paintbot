export interface Message {
    message?: String;
    time?: String;
    type?: MessageType;
}

export enum MessageType {
    sentTextMessage,
    receivedTextMessage
}
