import { Component, OnInit, Input } from '@angular/core';
import { Message, MessageType } from 'src/app/chat-bot/shared/message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  @Input() message: Message = {};
  positionClass: string;
  owner: string;
  constructor() { }

  ngOnInit() {
    if ( this.message.type === MessageType.sentTextMessage) {
      this.positionClass = 'toRight';
      this.owner = 'Ja';
    } else {
      this.positionClass = 'toLeft';
      this.owner = 'Paint Bot';
    }
  }

  getMessageClass(): string {
    switch (this.message.type) {
      case MessageType.sentTextMessage:
      return 'sentTextMessage commonMessage';

      case MessageType.receivedTextMessage:
      return 'receivedTextMessage commonMessage';
    }
  }

}



