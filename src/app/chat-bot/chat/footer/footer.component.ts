import { Component, OnInit } from '@angular/core';
import { ChatBotService } from '../../shared/chat-bot.service';
import { Message, MessageType } from '../../shared/message';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  message: String = '';
  constructor(private botService: ChatBotService) { }

  ngOnInit() {
  }

  sendMessage() {
    if (this.message.replace(/\s/g, '').length) {
      const message: Message = {
        message: this.message,
        time: Date(),
        type: MessageType.sentTextMessage
      };

      this.botService.sentMessage(message);
      this.message = '';
    }
  }

}
