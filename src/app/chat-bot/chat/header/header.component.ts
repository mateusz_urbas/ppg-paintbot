import { Component, OnInit } from '@angular/core';
import { ChatBotService } from '../../shared/chat-bot.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(protected chatService: ChatBotService) {
  }

  ngOnInit() {
  }

}
