import { Component, OnInit } from '@angular/core';
import { ChatBotService } from './shared/chat-bot.service';
import { trigger, state, style, transition, group, animate } from '@angular/animations';

@Component({
  selector: 'app-chat-bot',
  templateUrl: './chat-bot.component.html',
  styleUrls: ['./chat-bot.component.scss'],
})
export class ChatBotComponent implements OnInit {

  constructor(protected chatService: ChatBotService) { }

  ngOnInit() {
  }

  activeBot() {
    this.chatService.activeChatBot();
  }



}
