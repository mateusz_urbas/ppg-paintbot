import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';

import { ChatBotService } from './shared/chat-bot.service';

import { ChatBotComponent } from './chat-bot.component';
import { ChatComponent } from './chat/chat.component';
import { HeaderComponent } from './chat/header/header.component';
import { BodyComponent } from './chat/body/body.component';
import { FooterComponent } from './chat/footer/footer.component';
import { FormsModule } from '@angular/forms';
import { MessageComponent } from './chat/body/message/message.component';



@NgModule({
  declarations: [
    ChatBotComponent,
    ChatComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    MessageComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    FormsModule
  ],
  exports: [
    ChatBotComponent
  ],
  providers: [
    ChatBotService
  ]
})
export class ChatBotModule { }
